// Exercício #03:
// Joaozinho quer calcular e mostrar a quantidade de litros de combustível gastos em uma viagem, ao utilizar um automóvel que faz 12 KM/L. Para isso, ele gostaria que você o auxiliasse através de um simples programa. Para efetuar o cálculo, deve-se fornecer o tempo gasto na viagem (em horas) e a velocidade média durante a mesma (em km/h). Assim, pode-se obter distância percorrida e, em seguida, calcular quantos litros seriam necessários. Aproxime o valor para cima.
// Entrada
// O arquivo de entrada contém dois inteiros. O primeiro é o tempo gasto na viagem (em horas) e o segundo é a velocidade média durante a mesma (em km/h).

// Saída
// Imprima a quantidade de litros necessária para realizar a viagem, com o valor aproximado para cima.

function calculator(tempo, velocidade) {
    litros = Math.trunc(tempo/velocidade) / 12
    console.log(`Foram gastos ${litros} litros nessa viagem`)
}

tempo = 36
velocidade = 1
calculator(tempo, velocidade)