// Exercício #01:
// Leia um valor inteiro, que é o tempo de duração em segundos de um determinado evento em uma fábrica, e informe-o expresso no formato horas:minutos:segundos.

// Entrada
// O arquivo de entrada contém um valor inteiro N.

// Saída
// Imprima o tempo lido no arquivo de entrada (segundos), convertido para horas:minutos:segundos.

const relogio = function(valor) {
    
    horas = Math.trunc(valor / 3600)
    minutos = Math.trunc((valor - (horas * 3600))/ 60)
    segundos = Math.trunc((valor -  (horas * 3600)) - minutos * 60)

    console.log(`Esse valor corresponde a ${horas}horas, ${minutos}minutos e ${segundos}segundos`)
}

relogio(40000)