// Exercício #02:
// Você deve fazer um programa que leia um valor qualquer e apresente uma mensagem dizendo em qual dos seguintes intervalos ([0,25], (25,50], (50,75], (75,100]) este valor se encontra. Obviamente se o valor não estiver em nenhum destes intervalos, deverá ser impressa a mensagem “Fora de intervalo”.
// O símbolo ( representa "maior que". Por exemplo:
// [0,25]  indica valores entre 0 e 25.0000, inclusive eles.
// (25,50] indica valores maiores que 25 Ex: 25.00001 até o valor 50.0000000

// Entrada
// O arquivo de entrada contém um número qualquer (inteiro ou decimal).

// Saída
// A saída deve printar o intervalo que o valor está.

function VerificaIntervalo(num){
    if (0 <= num && num <= 100) {
      if (0 <= num && num <= 25) {
        return console.log(`o número ${num} está entre 0 e 25`)
      }
      else if (25 < num && num <= 50) {
        return console.log(`o número ${num} está entre 25 e 50`)
      }
      else if (50 < num && num <= 75) {
        return console.log(`o número ${num} está entre 50 e 75`)
      }
      else if (75 < num && num <= 100) {
        return console.log(`o número ${num} está entre 75 e 100`)
      }
    }
    else {
      return console.log("Fora de intervalo");
    }
  }

  VerificaIntervalo(45)